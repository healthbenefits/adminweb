package ui.smoke.testcases.adminWeb;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.killBrowserExe;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.adminWeb_Pages.HeaderPage;
import pages.adminWeb_Pages.LoginPage;
import pages.adminWeb_Pages.LoginPageMFA;
import pages.adminWeb_Pages.SSN_ResultsPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC1_loginToAdminWeb extends InitTests {
	public  String MFAChoice = "";
	public  String MFAEmailId = "";
	public  String MFAEmailPassword = "";
	public TC1_loginToAdminWeb(String appName) {
		super(appName);
	}
	
	@Test(enabled = true, priority = 1)
	public void loginToAdminWebUsingMFA() throws Exception {
		try {
			
			props.load(input);
			MFAChoice = props.getProperty("MFAChoiceiMercer");
			MFAEmailId = props.getProperty("AdminWebMFAEmailId");
			MFAEmailPassword = props.getProperty("AdminWebMFAEmailPassword");
			System.out.println(MFAEmailId+">>>"+MFAEmailPassword);
		test = reports.createTest("Logging in to admin web application with MFA");
		test.assignCategory("smoke");
		
		TC1_loginToAdminWeb adminWebTest = new TC1_loginToAdminWeb("AdminWebMFA");

		System.out.println("Logging in to application using MFA");
//		String url,String browserType,String browserVersion,String platform,String executionEnv,ExtentTest test,String nodeUrl
		initWebDriver(BASEURL,"IE", "", "", "saucelabs", test, "");
		System.out.println("BaseURL is: " + BASEURL);

		LoginPageMFA loginPage = new LoginPageMFA();
		SoftAssertions.assertTrue(isElementExisting(driver, LoginPageMFA.username_input, 5),"User has reached a login page", test);
		loginPage.loginToappMFA(USERNAME, PASSWORD);
		
		loginPage.enterMFA(MFAEmailId, MFAEmailPassword);
		
		HeaderPage headerPage = new HeaderPage();
		SoftAssertions.assertTrue(isElementExisting(driver, HeaderPage.logout_button, 5),"User logged in to application", test);
		
		headerPage.logout();
		SoftAssertions.assertTrue(isElementExisting(driver, LoginPageMFA.username_input, 5),"User logged out successfully", test);
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("MFA Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("MFA Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		
		finally {
			
			driver.close();
			killBrowserExe(BROWSER_TYPE);
		}
	}
	
	@Test(enabled = true, priority = 2)
	public void loginToAdminWeb() throws Exception {
		try {
		test = reports.createTest("Logging in to admin web application without MFA");
		test.assignCategory("smoke");
		
		TC1_loginToAdminWeb adminWebTest = new TC1_loginToAdminWeb("AdminWeb");

		System.out.println("Logging in to application");
//		String url,String browserType,String browserVersion,String platform,String executionEnv,ExtentTest test,String nodeUrl
		initWebDriver(BASEURL,"IE", "", "", "local", test, "");
		System.out.println("BaseURL is: " + BASEURL);

		LoginPage loginPage = new LoginPage();
		loginPage.loginToapp(USERNAME, PASSWORD);
		
		HeaderPage headerPage = new HeaderPage();
		SoftAssertions.assertTrue(isElementExisting(driver, HeaderPage.logout_button, 5),"User logged in to application", test);
	
		headerPage.logout();
		SoftAssertions.assertTrue(isElementExisting(driver, LoginPage.username_input, 5),"User logged out successfully", test);
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		
	}
	
	@Test(enabled = true, priority = 3)
	public void performSSNSearch() throws Exception {
		try {
		test = reports.createTest("Search using SSN");
		test.assignCategory("smoke");

		LoginPage loginPage = new LoginPage();
		loginPage.loginToapp(USERNAME, PASSWORD);
	
		HeaderPage headerPage = new HeaderPage();
		SoftAssertions.assertTrue(isElementExisting(driver, HeaderPage.logout_button, 5),"User is logged in to application", test);
		
		headerPage.searchSSN();
		
		SSN_ResultsPage resultsPage = new SSN_ResultsPage();
		SoftAssertions.assertTrue(isElementExisting(driver, SSN_ResultsPage.resultsPage_input, 5),"Results are displayed successfully", test);
		
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Failed to fetch information based on SSN", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Failed to fetch information based on SSN", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@AfterSuite
	public void killDriver() {
		reports.flush();
		driver.close();
		killBrowserExe(BROWSER_TYPE);
		
	}
}
