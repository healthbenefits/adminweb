package ui.smoke.testcases.adminWeb;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.adminWeb_Pages.CalculationsPage;
import pages.adminWeb_Pages.HeaderPage;
import pages.adminWeb_Pages.ResultsHeaderPage;
import pages.adminWeb_Pages.SSN_ResultsPage;
import verify.SoftAssertions;

public class TC2_estimateCalculations {
	@Test(enabled = true, priority = 1)
	public void estimateCalculations() throws Exception {
		try {
		test = reports.createTest("Calculation estimation testing");
		test.assignCategory("smoke");
		
		HeaderPage headerPage = new HeaderPage();
		SoftAssertions.assertTrue(isElementExisting(driver, HeaderPage.logout_button, 5),"User is logged in to application", test);
		
		headerPage.searchSSN();
		
		SSN_ResultsPage resultsPage = new SSN_ResultsPage();
		SoftAssertions.assertTrue(isElementExisting(driver, SSN_ResultsPage.resultsPage_input, 5),"Results are displayed successfully", test);
		
		ResultsHeaderPage rHeaderPage = new ResultsHeaderPage();
		rHeaderPage.navigateViaMenu(ResultsHeaderPage.calculation_menu, ResultsHeaderPage.request_submenu);
		
		CalculationsPage calculationPage = new CalculationsPage();
		SoftAssertions.assertTrue(isElementExisting(driver, CalculationsPage.process_dropdown , 5),"Page for calculation intiation is displayed", test);
		
		calculationPage.fillData();
		calculationPage.waitForCalculations();
		String basewindow = driver.getWindowHandle();
		calculationPage.accessParticipationReport();
		
		//Verifying if the new tab is opened
		driver.switchTo().window(basewindow);
		
		
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Failed to fetch information based on SSN", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Failed to fetch information based on SSN", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
}
