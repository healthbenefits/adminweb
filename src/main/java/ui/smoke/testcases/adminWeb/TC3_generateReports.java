package ui.smoke.testcases.adminWeb;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.switchToWindow;
import static driverfactory.Driver.delay;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.adminWeb_Pages.HeaderPage;
import pages.adminWeb_Pages.ReportsPage;
import pages.adminWeb_Pages.ResultsHeaderPage;
import pages.adminWeb_Pages.SSN_ResultsPage;
import verify.SoftAssertions;

public class TC3_generateReports {
	
	@Test(enabled = true, priority = 1)
	public void generateReport() throws Exception {
		try {
		test = reports.createTest("Generating reports");
		test.assignCategory("smoke");
		
		HeaderPage headerPage = new HeaderPage();
		SoftAssertions.assertTrue(isElementExisting(driver, HeaderPage.logout_button, 5),"User is logged in to application", test);
		
		headerPage.searchSSN();
		
		SSN_ResultsPage resultsPage = new SSN_ResultsPage();
		SoftAssertions.assertTrue(isElementExisting(driver, SSN_ResultsPage.resultsPage_input, 5),"Results are displayed successfully", test);
		
		ResultsHeaderPage rHeaderPage = new ResultsHeaderPage();
		rHeaderPage.navigateViaMenu(ResultsHeaderPage.tools_menu, ResultsHeaderPage.reports_submenu);
		rHeaderPage.navigateToReportsPage();

		ReportsPage reportsPage = new ReportsPage();
		SoftAssertions.assertTrue(isElementExisting(driver, ReportsPage.standardReports_link, 5),"Reports page is displayed", test);
		
		reportsPage.navigateToDeferredReports();
		SoftAssertions.assertTrue(isElementExisting(driver, ReportsPage.startDate_input, 5),"Deferred Reports page is displayed", test);
	
		reportsPage.fillData();
		System.out.println("File is opened");
		//report file is opened in the system editor.
		
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Failed to generate reports", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Failed to generate reports", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

}
