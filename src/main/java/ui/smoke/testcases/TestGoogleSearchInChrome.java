package ui.smoke.testcases;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;


import pages.DashBoardPage;
import pages.GoogleHomePage;
import utilities.InitTests;
import verify.SoftAssertions;

import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

public class TestGoogleSearchInChrome extends InitTests {
	DashBoardPage home;

		@Test( enabled = true) 
		public void testSearchInChrome() throws Exception {
			Driver driverFact=new Driver();
			WebDriver chromeDriver = null;
			ExtentTest test=null;
			try {
				test = reports.createTest("testSearchInChrome");
				test.assignCategory("smoke");
				 chromeDriver=driverFact.initWebDriver( "https://www.google.co.in","chrome","","","local",test,"");
				GoogleHomePage ghomeChrome=new GoogleHomePage(chromeDriver);
				ghomeChrome.search("selenium");
				waitForElementToDisplay(ghomeChrome.getFirstLnk());
				verifyElementTextContains(ghomeChrome.getFirstLnk(), "selenium",test);

			} catch (Error e) {
				e.printStackTrace();
				SoftAssertions.fail(e, driverFact.getScreenPath(chromeDriver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				softAssert.assertAll();

				

			} catch (Exception e) {
				e.printStackTrace();
				SoftAssertions.fail(e,driverFact.getScreenPath(chromeDriver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				softAssert.assertAll();


			} 
			finally
			{
				reports.flush();
				chromeDriver.close();

			}
	}
	


}
