package pages.adminWeb_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HeaderPage {

	
	@FindBy(xpath ="//a[@href = 'logout.asp']")
	public static WebElement logout_button;
	
	@FindBy(xpath ="//input[@name = 'SSN']")
	public static WebElement ssn_input;
	
	@FindBy(xpath = "//input[@name = 'btn_go']")
	public static WebElement ssnGo_button;
	
	public HeaderPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void searchSSN() {
		setInput(ssn_input, "745242138");
		clickElement(ssnGo_button);
	}
	
	public void logout() {
		clickElement(logout_button);
	}
}
