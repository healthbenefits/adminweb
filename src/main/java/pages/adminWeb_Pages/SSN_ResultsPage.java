package pages.adminWeb_Pages;


import static driverfactory.Driver.driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SSN_ResultsPage {

	
	@FindBy(xpath ="//input[@name = 'lastname']")
	public static WebElement resultsPage_input;
	
	public SSN_ResultsPage() {
		PageFactory.initElements(driver, this);
	}
	
}
