package pages.adminWeb_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.clickElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	@FindBy(xpath = "//font[contains(text(), 'Welcome to the Plan Sponsor Site')]")
	public static WebElement loginPage_header;
	
	@FindBy(xpath = "//input[@name='username']")
	public static WebElement username_input;
	
	@FindBy(xpath = "//input[@name = 'password']")
	public static WebElement password_input;
	
	@FindBy(xpath = "//input[@type = 'submit']")
	public static WebElement login_button;
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void loginToapp(String username, String password) {
		setInput(username_input, username);
		setInput(password_input, password);
		clickElement(login_button);		
	}
}
