package pages.adminWeb_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.hoverAndClickOnElement;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.switchToWindowWithURL;
import java.util.ArrayList;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ResultsHeaderPage {

	@FindBy(xpath = "//a[contains(text(), 'Calculation')]")
	public static WebElement calculation_menu;

	@FindBy(id = "menuItemText6")
	public static WebElement request_submenu;

	@FindBy(xpath = "//a[contains(text(), 'Calculation')]/parent::td/following-sibling::td/a[contains(text(), 'Tools')]")
	public static WebElement tools_menu;

	@FindBy(id = "menuItemText0")
	public static WebElement reports_submenu;

	public ResultsHeaderPage() {
		PageFactory.initElements(driver, this);
	}

	public void navigateViaMenu(WebElement menu, WebElement subMenu) {

		hoverOverElement(driver, menu);
		hoverAndClickOnElement(driver, menu, subMenu);
		
	}

	public void navigateToReportsPage() {
		delay(6000);
		driver.get("http://usfkl13as279v.mrshmc.com/QAIKGGAdmin/Reports/Reports_Home.aspx");
//		switchToWindowWithURL("http://usfkl13as279v.mrshmc.com/QAIKGGAdmin/Reports/Reports_Home.aspx");
	}
}
