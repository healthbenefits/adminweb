package pages.adminWeb_Pages;


import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.selEleByValue;
import static driverfactory.Driver.clickElementUsingJavaScript;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ReportsPage {

	@FindBy(xpath = "//a[contains(text(), 'Standard Reports')]")
	public static WebElement standardReports_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Deferred Vested')]")
	public static WebElement deferredVestedReports_link;
	
	@FindBy(id = "_ctl0_txtStartingOn")
	public static WebElement startDate_input;
	
	@FindBy(id = "_ctl0_txtEndingOn")
	public static WebElement endDate_input;
	
	@FindBy(id = "_ctl0_lstPlanCodes")
	public static WebElement inPlan_select;
	
	@FindBy(id = "_ctl0_lstOrgUnit")
	public static WebElement organizationalUnit_select;
	
	@FindBy(id = "_ctl0_cboOutputType")
	public static WebElement outputType_select;
	
	@FindBy(id = "_ctl0_RunReport")
	public static WebElement runReport_button;
	
	public ReportsPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void navigateToDeferredReports() {
		clickElement(standardReports_link);
		clickElement(deferredVestedReports_link);
	}
	
	public void fillData() {
		setInput(startDate_input, "01/01/2018");
		setInput(endDate_input, "11/22/2018");
		selEleByValue(inPlan_select, "1");
		selEleByValue(organizationalUnit_select, "1");
		selEleByValue(outputType_select, "PDF");
		clickElementUsingJavaScript(driver, runReport_button);
		System.out.println("clicked on run report");
	}
}

