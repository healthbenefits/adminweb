package pages.adminWeb_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.selEleByValue;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CalculationsPage {

	@FindBy(xpath = "//select[@name = 'processNm']")
	public static WebElement process_dropdown;
	
	@FindBy(xpath = "//input[@value = 'E']")
	public static WebElement estimate_radiobutton;
	
	@FindBy(xpath = "//input[@name = 'terminationdate']")
	public static WebElement determinationDate_input;
	
	@FindBy(xpath = "//input[@name = 'annuitydate']")
	public static WebElement commencementDate_input;
	
	@FindBy(xpath = "//select[@name = 'plancdrequested']")
	public static WebElement plan_dropdown;
	
	@FindBy(xpath = "//input[@value = ' Calculate ']")
	public static WebElement calculate_button;
	
	@FindBy(xpath = "//div[contains(@text, 'CalculationId')]")
	public static WebElement calculationSuccess_component;
	
	@FindBy(xpath = "//span[contains(text(), '[+]')]")
	public static WebElement expansion_toggle;
	
	@FindBy(xpath = "(//span[contains(text(), '[+]')])[5]")
	public static WebElement participationWorksheet_toggle;

	@FindBy(xpath = "//td[contains(text(), 'Calculation Worksheet - Internal')]//following-sibling::td/a")
	public static WebElement calculationWorksheet_viewLink;
	
	
	public CalculationsPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void fillData() {
		selEleByVisbleText(process_dropdown, "0Main");
		clickElement(estimate_radiobutton);
		setInput(determinationDate_input, "08/25/2018");
		setInput(commencementDate_input, "12/25/2018");
		selEleByValue(plan_dropdown, "11");
		clickElement(calculate_button);
	}
	
	public void waitForCalculations() {
		boolean found = false;
		int i = 0;
	if(found == false && i<10) {
		delay(3000);
		found = driver.findElements(By.xpath("//div[contains(@text, 'CalculationId')]")).size()>0;
		if(found) {
			System.out.println("Displayed");
		}
		delay(3000);
		i++;
	}
	else {
		System.out.println("Too much time taken by the calulations");
	}
	}
	
	public void accessParticipationReport() {
		clickElement(expansion_toggle);
		clickElement(participationWorksheet_toggle);
		clickElement(calculationWorksheet_viewLink);	
		
	}
}
